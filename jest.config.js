module.exports = {
  projects: [
    '<rootDir>/apps/psa',
    '<rootDir>/apps/psa-frontend',
    '<rootDir>/libs/ordenes',
    '<rootDir>/libs/auth',
    '<rootDir>/libs/prime',
    '<rootDir>/libs/plovs',
    '<rootDir>/libs/plantilla',
  ],
};
