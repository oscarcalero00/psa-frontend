import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridOrdenComponent } from './grid-orden.component';

describe('GridOrdenComponent', () => {
  let component: GridOrdenComponent;
  let fixture: ComponentFixture<GridOrdenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridOrdenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridOrdenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
