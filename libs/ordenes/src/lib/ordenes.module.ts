import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridOrdenComponent } from './components/grid-orden/grid-orden.component';
import { FormOrdenComponent } from './components/form-orden/form-orden.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    GridOrdenComponent,
    FormOrdenComponent
  ],
})
export class OrdenesModule {}
